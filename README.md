# Practica: Contabilidad

## Enunciado

Se desea crear un programa que procese ficheros aprovechando el paralelismo de la máquina. Se tienen cinco ficheros con los siguientes nombres:

informatica.txt
gerencia.txt
contabilidad.txt
comercio.txt
rrhh.txt

* En cada fichero hay una lista de 12 cantidades enteras que representan las contabilidades de distintos departamentos de los últimos 12 meses.
* Hay una cantidad en cada línea, 12 por fichero.
* Se desea que el programa creado sume la cantidad total que suman todas las cantidades de los cinco ficheros haciendo uso de Threads.

## Aspectos a tener en cuenta

* Generar un fichero de resultados por cada fichero de datos y llamarlos igual pero con la extension ".res".; es decir, la suma de las cantidades de informatica.txt se dejará en informatica.res.

* Una vez se hayan generado todos los ficheros con los resultados, se deberán sumar todas las cantidades de cada uno de los ficheros y mostrar el resultado por pantalla

* Para hacer este paso es necesario que todos los ficheros .res se hayan generado. Para ello podemos hacer uso de los métodos [java.lang.Thread.join](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html#join--) o [java.lang.Thread.isAlive()](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html#isAlive--)
